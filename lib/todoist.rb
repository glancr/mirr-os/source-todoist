# frozen_string_literal: true

require 'todoist/engine'
require 'httparty'

module Todoist
  class Hooks
    REFRESH_INTERVAL = '5m'

    # @return [String]
    def self.refresh_interval
      REFRESH_INTERVAL
    end

    # @param [Hash] configuration
    def initialize(instance_id, configuration)
      @instance_id = instance_id
      @api_token = configuration['apiToken']
    end

    def default_title
      res = HTTParty.post(
        sync_url,
        body: post_body('["user"]')
      ).parsed_response
      "#{res['user']['full_name']} (#{res['user']['email']})"
    end

    def configuration_valid?
      # TODO: Add credential validation if present.
      fetch_or_raise endpoint: 'projects'
      true
    rescue ArgumentError, HTTParty::ResponseError
      false
    end

    def list_sub_resources
      res = fetch_or_raise endpoint: 'projects'
      res.map { |project| [project['id'], project['name']] }
    end

    # @param [String] _group
    # @param [Array<String>] sub_resources
    def fetch_data(_group, sub_resources)
      records = []
      list_map = {}
      fetch_or_raise(endpoint: 'projects').each do |project|
        list_map[project['id'].to_s] = project['name']
      end
      # FIXME: mirr.OS should provide cached options to look up, see https://gitlab.com/glancr/mirros_api/issues/120

      sub_resources.each do |project_id|
        list = ReminderList
               .includes(:reminders)
               .find_or_initialize_by(id: project_id) do |reminder_list|
          reminder_list.id = project_id
        end
        list.name = list_map[project_id]

        tasks = fetch_or_raise endpoint: 'tasks',
                               query: { project_id: project_id }
        current_task_ids = tasks.map { |task| task['id'].to_s }
        list.reminders.reject { |task| current_task_ids.include?(task.uid) }.each(&:mark_for_destruction)

        tasks.each do |task|
          attributes = {
            uid: task['id'],
            creation_date: task['created'],
            # Use datetime if available, expression returns nil in case there is no due date at all.
            due_date: task.dig('due', 'datetime') || task.dig('due', 'date'),
            summary: task['content']&.length > 255 ? "#{task['content']&.slice(0, 254)}…" : task['content'], # FIXME: Remove markdown for links
            completed: task['completed']
            # TODO: Add assignee support
          }
          list.update_or_insert_child(task['id'], attributes)
        end

        records << list
      end
      records
    end

    private

    def fetch_or_raise(options)
      res = HTTParty.get(rest_url(options), auth_headers)
      raise HTTParty::ResponseError unless res.success?

      res.parsed_response
    end

    def auth_headers
      raise ArgumentError, 'Access token not present' if @api_token.blank?

      { headers: { Authorization: "Bearer #{@api_token}" } }
    end

    # @param [Hash] options
    # @option [String] endpoint Todoist REST API endpoint to call
    # @option [String] query Optional query parameters
    def rest_url(options = { endpoint: 'projects', query: nil })
      URI::HTTPS.build(
        host: 'api.todoist.com',
        path: "/rest/v1/#{options[:endpoint]}",
        query: options[:query]&.to_query
      )
    end

    def sync_url
      URI::HTTPS.build(
        host: 'api.todoist.com',
        path: '/sync/v8/sync'
      )
    end

    # @param [String] resource_types valid Todoist resource types in the syntax '["type1", "type2"]'
    def post_body(resource_types = "['user']")
      { token: @api_token, sync_token: '*', resource_types: resource_types }
    end
  end
end
