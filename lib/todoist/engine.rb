module Todoist
  class Engine < ::Rails::Engine
    isolate_namespace Todoist
    config.generators.api_only = true
  end
end
