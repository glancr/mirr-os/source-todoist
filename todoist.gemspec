require 'json'

$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'todoist/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'todoist'
  s.version     = Todoist::VERSION
  s.authors     = ['Tobias Grasse']
  s.email       = ['tg@glancr.de']
  s.homepage    = 'https://glancr.de/modules/todoist'
  s.summary     = 'mirr.OS data source for tasks from Todoist.'
  s.description = 'Fetches your current tasks from Todoist.'
  s.license     = 'MIT'
  s.metadata    = { 'json' =>
                {
                  type: 'sources',
                  title: {
                    enGb: 'Todoist'
                  },
                  description: {
                    enGb: s.description,
                    deDe: 'Lädt deine aktuellen Aufgaben von Todoist.',
                    frFr: 'Récupère vos tâches actuelles de Todoist.',
                    esEs: 'Recupera tus tareas actuales de Todoist.',
                    plPl: 'Pobiera bieżące zadania z Todoist.',
                    koKr: 'Todoist에서 현재 작업을 가져옵니다.'
                  },
                  groups: [:reminder_list]
                }.to_json
              }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails'
  s.add_dependency 'httparty', '~> 0.18'
end
